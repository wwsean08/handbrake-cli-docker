FROM ubuntu:18.04
RUN apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository ppa:stebbins/handbrake-releases && \
    apt-get update && \
    apt-get install -y handbrake-cli ca-certificates && \
    apt-get clean all && rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*
